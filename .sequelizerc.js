'use strict';

require('dotenv').config();

module.exports = {
  development: {
    username: 'user',
    password: 'password',
    database: 'testdb',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
};
