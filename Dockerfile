FROM node:12-alpine as builder

WORKDIR /app
COPY package*.json ./
COPY src src
COPY .sequelize* ./
COPY tsconfig.json ./


RUN npm ci
RUN npm run build
RUN npm prune

FROM node:12-alpine

WORKDIR /app
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package*.json ./
COPY --from=builder /app/tsconfig.json ./
COPY --from=builder /app/.sequelize* ./

EXPOSE 3000

CMD ["node", "dist/index.js"]
