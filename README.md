# test-backend

1. start docker services by `cd docker && docker-compose up -d`
2. install dependencies by `npm install`
3. run the postgres migration by `npm run migration`
4. start the app in dev mode `npm run start:dev` or build the sources `npm run build` and start production mode `npm run start`
5. check the documentation at `/documentation` route
6. You can find the SQL dump under `data/dump.pgsql` but if you ran the migration you don't have to apply the dump

Services - find the credentials under `/docker/docker-compose.yml` file
1. access pgadmin by navigate (http://localhost:5050) in your browser 
2. access minio (s3 storage) by navigate (http://localhost:9000) in your browser
