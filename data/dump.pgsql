--
-- PostgreSQL database dump
--

-- Dumped from database version 10.13 (Debian 10.13-1.pgdg90+1)
-- Dumped by pg_dump version 10.13 (Debian 10.13-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO "user";

--
-- Name: todos; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.todos (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "createdAt" timestamp with time zone DEFAULT '2020-07-28 16:19:11.513+00'::timestamp with time zone,
    "updatedAt" timestamp with time zone DEFAULT '2020-07-28 16:19:11.513+00'::timestamp with time zone,
    "userId" uuid NOT NULL,
    title text,
    date timestamp with time zone
);


ALTER TABLE public.todos OWNER TO "user";

--
-- Name: users; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.users (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "createdAt" timestamp with time zone DEFAULT '2020-07-28 16:19:11.513+00'::timestamp with time zone,
    "updatedAt" timestamp with time zone DEFAULT '2020-07-28 16:19:11.513+00'::timestamp with time zone,
    email text,
    name text,
    "phoneNumber" text,
    image text
);


ALTER TABLE public.users OWNER TO "user";

--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public."SequelizeMeta" (name) FROM stdin;
202007241200-initial-migration.js
\.


--
-- Data for Name: todos; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.todos (id, "createdAt", "updatedAt", "userId", title, date) FROM stdin;
d7be8f09-6de7-4058-84f5-8c91bdd02553    2020-07-28 16:35:58.322+00      2020-07-28 16:35:17.924+00      071003bd-6963-4c32-879f-0c694d8fc4b1    Example Todo    2020-07-17 16:35:55+00
ac5108e8-08ec-42e1-b482-55e11a708578    2020-07-28 16:43:59.868+00      2020-07-28 16:35:17.924+00      071003bd-6963-4c32-879f-0c694d8fc4b1    Test todo       2020-07-24 16:43:36+00
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.users (id, "createdAt", "updatedAt", email, name, "phoneNumber", image) FROM stdin;
071003bd-6963-4c32-879f-0c694d8fc4b1    2020-07-28 16:35:46.278+00      2020-07-28 16:35:17.924+00      test@test.com   Test User       063012345678    e9de5f17-3c49-4e83-b600-c973932e79e3
\.


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: todos todos_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (id);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: todos todos_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.todos
    ADD CONSTRAINT "todos_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--
