import { FastifyInstance } from 'fastify';
import * as Minio from 'minio';
import { OpenAPIV3 } from 'openapi-types';
import { ConfigObject } from './config';
import { MinioImageService } from './domain/image/image-minio.service';
import { TodoSequelizeRepository } from './domain/todo/todo-sequelize.repository';
import { setTodoModelReferences, todoModelInit } from './domain/todo/todo.model';
import { UserSequelizeRepository } from './domain/user/user-sequelize.repository';
import { setUserModelReferences, userModelInit } from './domain/user/user.model';
import { StatusEndpoint } from './endpoints/status/status.endpoint';
import { CreateTodoEndpoint } from './endpoints/todo/create-todo.endpoint';
import { DeleteTodoEndpoint } from './endpoints/todo/delete-todo.endpoint';
import { GetTodoEndpoint } from './endpoints/todo/get-todo.endpoint';
import { GetTodosByUserIdEndpoint } from './endpoints/todo/get-todos-by-userid.endpoint';
import { UpdateTodoEndpoint } from './endpoints/todo/update-todo.endpoint';
import { ChangeUserImageEndpoint } from './endpoints/user/change-user-image.endpoint';
import { CreateUserEndpoint } from './endpoints/user/create-user.endpoint';
import { DeleteUserEndpoint } from './endpoints/user/delete-user.endpoint';
import { GetUserImageEndpoint } from './endpoints/user/get-user-image.endpoint';
import { GetUserEndpoint } from './endpoints/user/get-user.endpoint';
import { RequestUserImageEndpoint } from './endpoints/user/request-user-image.endpoint';
import { UpdateUserEndpoint } from './endpoints/user/update-user.endpoint';
import { Endpoint } from './framework/endpoint/endpoint';
import { FastifySever } from './framework/fastify/fastify-server.factory';
import { fastifySwaggerFactory } from './framework/fastify/fastify-swagger-factory';
import { sequelizeConnection } from './framework/sequelize/connection';
import { Logger } from './logger';
import { GetAllUserEndpoint } from './endpoints/user/get-all-users.endpoint';

export const create = ({ logger, config }: { logger: Logger; config: ConfigObject }): FastifyInstance => {
  const sequelize = sequelizeConnection(false);

  // init of the repos should happen before using the repos
  userModelInit({ sequelize });
  todoModelInit({ sequelize });

  // setting the references should happen after all the model has been registered
  setUserModelReferences();
  setTodoModelReferences();

  const securityRequirementObjects: OpenAPIV3.SecurityRequirementObject[] = [
    {
      bearerToken: [],
    },
  ];

  const components: OpenAPIV3.ComponentsObject = {
    schemas: {},
    securitySchemes: {
      bearerToken: {
        type: 'http',
        scheme: 'bearer',
      },
    },
  };

  const swaggerOptions = fastifySwaggerFactory({
    components,
    security: securityRequirementObjects,
    host: 'localhost',
  });

  const minio = new Minio.Client({
    endPoint: config.minio.endpoint,
    port: config.minio.port,
    useSSL: false,
    accessKey: config.minio.accessKeyId,
    secretKey: config.minio.secretAccessKey,
  });

  const userRepository = new UserSequelizeRepository();
  const todoRepository = new TodoSequelizeRepository();

  const imageService = new MinioImageService(minio, {
    expiresIn: config.minio.expiresIn,
    bucket: config.minio.bucketName,
  });

  const statusEndpoint = new StatusEndpoint();
  const createUserEndpoint = new CreateUserEndpoint(userRepository);
  const getUserEndpoint = new GetUserEndpoint(userRepository, config.domain);
  const deleteUserEndpoint = new DeleteUserEndpoint(userRepository, todoRepository, imageService);
  const updateUserEndpoint = new UpdateUserEndpoint(userRepository);
  const createTodoEndpoint = new CreateTodoEndpoint(todoRepository, userRepository);
  const getTodoEndpoint = new GetTodoEndpoint(todoRepository);
  const deleteTodoEndpoint = new DeleteTodoEndpoint(todoRepository);
  const updateTodoEndpoint = new UpdateTodoEndpoint(todoRepository);
  const getTodosByUserIdEndpoint = new GetTodosByUserIdEndpoint(userRepository, todoRepository);
  const requestUserImageEndpoint = new RequestUserImageEndpoint(userRepository, imageService);
  const changeUserImageEndpoint = new ChangeUserImageEndpoint(userRepository, imageService);
  const getUserImageEndpoint = new GetUserImageEndpoint(imageService);
  const getAllUsersEndpoint = new GetAllUserEndpoint(userRepository, config.domain);

  const endpoints: Endpoint[] = [
    statusEndpoint,
    createUserEndpoint,
    getUserEndpoint,
    deleteUserEndpoint,
    updateUserEndpoint,
    createTodoEndpoint,
    getTodoEndpoint,
    deleteTodoEndpoint,
    updateTodoEndpoint,
    getTodosByUserIdEndpoint,
    requestUserImageEndpoint,
    changeUserImageEndpoint,
    getUserImageEndpoint,
    getAllUsersEndpoint,
  ];

  const fastifyServer = new FastifySever(endpoints, swaggerOptions, logger);
  const server = fastifyServer.getServer();

  server.addHook('onClose', async () => {
    await sequelize.close();
  });

  return server;
};

export const start = async ({ config, logger }: { config: ConfigObject; logger: Logger }): Promise<void> => {
  const server = create({ logger, config });
  await server.listen(config.port, '0.0.0.0');
  logger.info(`app listening on port ${config.port}`);
  await server.oas();
};
