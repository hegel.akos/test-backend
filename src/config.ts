import convict from 'convict';
import * as dotenv from 'dotenv';

dotenv.config();
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

type MinioConfig = {
  accessKeyId: string;
  secretAccessKey: string;
  bucketName: string;
  endpoint: string;
  expiresIn: number;
  signature: string;
  port: number;
  region: string;
};

export type ConfigObject = {
  domain: string;
  port: number;
  logger: {
    level: string;
  };
  cors: {
    allowedOrigins: string;
  };
  minio: MinioConfig;
};

const convictConfig = convict<ConfigObject>({
  domain: {
    doc: 'The domain of the app',
    format: String,
    default: 'http://localhost:4000',
    env: 'APP_DOMAIN',
  },
  port: {
    doc: 'The port to bind.',
    format: Number,
    default: 4000,
    env: 'PORT',
  },
  logger: {
    level: {
      doc: 'Defines the level of the logger.',
      format: String,
      default: 'debug',
      env: 'LOGGER_LEVEL',
    },
  },
  cors: {
    allowedOrigins: {
      doc: 'Allowed origins.',
      format: Number,
      default: '*',
      env: 'CORS_ALLOWED_ORIGINS',
    },
  },
  minio: {
    expiresIn: {
      doc: 'Signed url expiration time',
      format: Number,
      default: 60 * 5,
      env: 'MINIO_EXPIRATION_TIME',
    },
    port: {
      doc: 'Minio access port',
      format: Number,
      default: 9000,
      env: 'MINIO_PORT',
    },
    signature: {
      doc: 'Minio signature version',
      format: String,
      default: 'v4',
      env: 'MINIO_SIGNATURE_VERSION',
    },
    accessKeyId: {
      doc: 'The public key used to access the MINIO storage for images',
      format: String,
      default: 'admin',
      env: 'MINIO_IMAGE_ACCESS_KEY',
    },
    secretAccessKey: {
      doc: 'The private key used to access the MINIO storage for images',
      format: String,
      default: 'password',
      env: 'MINIO_IMAGE_SECRET_ACCESS_KEY',
    },
    bucketName: {
      doc: 'The bucket name where to store images',
      format: String,
      default: 'bucket',
      env: 'MINIO_IMAGE_S3_BUCKET_NAME',
    },
    endpoint: {
      doc: 'The server where to store images',
      format: String,
      default: 'localhost',
      env: 'MINIO_IMAGE_S3_SERVER_ENDPOINT',
    },
    region: {
      doc: 'The region where the s3 resides in',
      format: String,
      default: 'eu-central-1',
      env: 'MINIO_IMAGE_S3_REGION',
    },
  },
});

convictConfig.validate({ allowed: 'strict' });

export const config = convictConfig.getProperties();
