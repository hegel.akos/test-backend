import * as Minio from 'minio';
import { Image, ImageService } from './image.service';
import { ResourceNotFound } from '../../framework/errors/resource.errors';

export class MinioImageService implements ImageService {
  constructor(private minio: Minio.Client, private config: { expiresIn: number; bucket: string }) {}

  public async check(ref: string): Promise<void> {
    try {
      await this.minio.getObject(this.config.bucket, ref);
    } catch (err) {
      if (err && err.code === 'NotFound') {
        throw new ResourceNotFound('image');
      }
      throw err;
    }
  }

  public async read(ref: string): Promise<Image> {
    const signedUrl = await this.minio.presignedGetObject(this.config.bucket, ref);
    return { signedUrl };
  }

  public async put(ref: string): Promise<Image> {
    const signedUrl = await this.minio.presignedPutObject(this.config.bucket, ref);
    return { signedUrl };
  }

  public async delete(ref: string): Promise<void> {
    await this.minio.removeObject(this.config.bucket, ref);
  }
}
