export type Image = {
  signedUrl: string;
};

export interface ImageService {
  check(ref: string): Promise<void>;
  read(ref: string): Promise<Image>;
  put(ref: string): Promise<Image>;
  delete(ref: string): Promise<void>;
}
