import { ResourceNotFound } from '../../framework/errors/resource.errors';
import { User } from '../user/user';
import { Todo, TodoUpdates } from './todo';
import { TodoModel } from './todo.model';
import { TodoRepository } from './todo.repository';

export class TodoSequelizeRepository implements TodoRepository {
  public transformer(todoModel: TodoModel): Todo {
    return {
      id: todoModel.id,
      createdAt: todoModel.createdAt,
      updatedAt: todoModel.updatedAt,
      title: todoModel.title,
      date: todoModel.date,
      userId: todoModel.userId,
    };
  }

  public async create(todo: Partial<Todo>): Promise<Todo> {
    const createdTodo = await TodoModel.create(todo);
    return this.transformer(createdTodo);
  }

  public async read(todoId: string): Promise<Todo> {
    const selectedTodo = await TodoModel.findOne({
      where: {
        id: todoId,
      },
    });
    if (!selectedTodo) {
      throw new ResourceNotFound('todo');
    }
    return this.transformer(selectedTodo);
  }

  public async readAll(): Promise<Todo[]> {
    const selectedTodos = await TodoModel.findAll();
    return selectedTodos.map((t) => this.transformer(t));
  }

  public async update(todoId: string, updates: TodoUpdates): Promise<Todo> {
    const selectedTodo = await TodoModel.findOne({
      where: {
        id: todoId,
      },
    });
    if (!selectedTodo) {
      throw new ResourceNotFound('todo');
    }
    const updatableTodo = selectedTodo.set({ ...updates, updatedAt: new Date() });
    const updatedTodo = await updatableTodo.save();
    return updatedTodo;
  }

  public async delete(todoId: string): Promise<void> {
    const selectedTodo = await TodoModel.findOne({
      where: {
        id: todoId,
      },
    });
    if (!selectedTodo) {
      throw new ResourceNotFound('todo');
    }
    await TodoModel.destroy({
      where: {
        id: todoId,
      },
    });
  }

  public async readTodosByUserId(userId: User['id']): Promise<Todo[]> {
    const selectedTodos = await TodoModel.findAll({
      where: {
        userId,
      },
    });
    return selectedTodos.map((t) => this.transformer(t));
  }

  public async deleteTodosByUserId(userId: User['id']): Promise<void> {
    await TodoModel.destroy({
      where: {
        userId,
      },
    });
  }
}
