import { DataTypes, ModelAttributes, Sequelize } from 'sequelize';
import { baseModelAttributes, ResourceModel } from '../../framework/sequelize/resource';
import { USERS_TABLE_NAME, UserModel } from '../user/user.model';

export const TODO_TABLE_NAME = 'todos';

const todoAttributes: ModelAttributes = {
  ...baseModelAttributes,
  userId: {
    type: DataTypes.UUID,
    references: {
      model: USERS_TABLE_NAME,
      key: 'id',
    },
    allowNull: false,
  },
  title: {
    type: DataTypes.TEXT,
  },
  date: {
    type: DataTypes.DATE,
  },
};

export class TodoModel extends ResourceModel {
  userId!: string;
  title!: string;
  date!: Date;
}

export const todoModelInit = ({ sequelize }: { sequelize: Sequelize }): void => {
  TodoModel.init(todoAttributes, { sequelize, tableName: TODO_TABLE_NAME, timestamps: true });
};

export const setTodoModelReferences = (): void => {
  TodoModel.belongsTo(UserModel, { foreignKey: 'id' });
};
