import { ResourceRepository } from '../../framework/sequelize/resource';
import { User } from '../user/user';
import { Todo, TodoUpdates } from './todo';
import { TodoModel } from './todo.model';

export interface TodoRepository extends ResourceRepository<Todo, TodoUpdates, TodoModel> {
  readTodosByUserId(userId: User['id']): Promise<Todo[]>;
  deleteTodosByUserId(userId: User['id']): Promise<void>;
}
