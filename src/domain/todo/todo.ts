import { Resource } from '../../framework/sequelize/resource';
import { User } from '../user/user';

export type Todo = Resource & {
  title: string;
  date: Date;
  userId: User['id'];
};

export type TodoUpdates = Partial<Omit<Todo, 'id' | 'createdAt' | 'updatedAt'>>;
