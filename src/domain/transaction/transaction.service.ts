export type Transaction = {
  commit(): Promise<void>;
  rollback(): Promise<void>;
};

export interface TransactionService {
  createTransaction(): Promise<Transaction>;
}
