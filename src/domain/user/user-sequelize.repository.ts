import { ResourceNotFound } from '../../framework/errors/resource.errors';
import { User, UserUpdates } from './user';
import { UserModel } from './user.model';
import { UserRepository } from './user.repository';
export class UserSequelizeRepository implements UserRepository {
  public transformer(userModel: UserModel): User {
    return {
      id: userModel.id,
      createdAt: userModel.createdAt,
      updatedAt: userModel.updatedAt,
      name: userModel.name,
      email: userModel.email,
      phoneNumber: userModel.phoneNumber,
      image: userModel.image,
    };
  }

  public async create(user: Partial<User>): Promise<User> {
    const createdUser = await UserModel.create(user);
    return createdUser;
  }

  public async read(userId: string): Promise<User> {
    const selectedUser = await UserModel.findOne({
      where: {
        id: userId,
      },
    });
    if (!selectedUser) {
      throw new ResourceNotFound('user');
    }
    return this.transformer(selectedUser);
  }

  public async readAll(): Promise<User[]> {
    const selectedUsers = await UserModel.findAll();
    return selectedUsers.map((u) => this.transformer(u));
  }

  public async update(userId: string, updates: UserUpdates): Promise<User> {
    const selectedUser = await UserModel.findOne({
      where: {
        id: userId,
      },
    });
    if (!selectedUser) {
      throw new ResourceNotFound('user');
    }
    const updatableUser = selectedUser.set({ ...updates, updatedAt: new Date() });
    const updatedUser = await updatableUser.save();
    return updatedUser;
  }

  public async delete(userId: string): Promise<void> {
    const selectedUser = await UserModel.findOne({
      where: {
        id: userId,
      },
    });
    if (!selectedUser) {
      throw new ResourceNotFound('user');
    }
    await UserModel.destroy({
      where: {
        id: userId,
      },
    });
  }
}
