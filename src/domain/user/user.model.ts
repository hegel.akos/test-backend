import { DataTypes, ModelAttributes, Sequelize } from 'sequelize';
import { baseModelAttributes, ResourceModel } from '../../framework/sequelize/resource';
import { TodoModel } from '../todo/todo.model';

export const USERS_TABLE_NAME = 'users';

const userAttributes: ModelAttributes = {
  ...baseModelAttributes,
  email: {
    type: DataTypes.TEXT,
    unique: true,
  },
  name: {
    type: DataTypes.TEXT,
  },
  phoneNumber: {
    type: DataTypes.TEXT,
  },
  image: {
    type: DataTypes.TEXT,
    allowNull: true,
    defaultValue: null,
  },
};

export class UserModel extends ResourceModel {
  public name!: string;
  public email!: string;
  public phoneNumber!: string;
  public image!: string | null;
  // public todos?: TodoModel[];
}

export const userModelInit = ({ sequelize }: { sequelize: Sequelize }): void => {
  UserModel.init(userAttributes, { sequelize, tableName: USERS_TABLE_NAME, timestamps: true });
};

export const setUserModelReferences = (): void => {
  UserModel.hasMany(TodoModel, { foreignKey: 'userId', as: 'todos' });
};
