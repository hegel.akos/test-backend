import { ResourceRepository } from '../../framework/sequelize/resource';
import { User, UserUpdates } from './user';
import { UserModel } from './user.model';

export interface UserRepository extends ResourceRepository<User, UserUpdates, UserModel> {}
