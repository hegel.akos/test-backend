import { Resource } from '../../framework/sequelize/resource';

export type User = Resource & {
  email: string;
  name: string;
  phoneNumber: string;
  image: string | null;
};
export type UserUpdates = Partial<Omit<User, 'email' | 'createdAt'>>;
