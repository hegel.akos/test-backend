import { Endpoint, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';
export class StatusEndpoint implements Endpoint<{}, {}, {}, {}, { status: string }> {
  public method = EndpointMethod.GET;
  public route = '/status';
  public schema: EndpointSchema = {
    tags: ['Status'],
    summary: 'Returns the status of the API.',
  };
  public handler() {
    return Promise.resolve({
      status: 200,
      response: {
        status: 'ok',
      },
    });
  }
}
