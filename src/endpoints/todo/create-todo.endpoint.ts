import { Todo } from '../../domain/todo/todo';
import { TodoRepository } from '../../domain/todo/todo.repository';
import { User } from '../../domain/user/user';
import { UserRepository } from '../../domain/user/user.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';
import { v4 } from 'uuid';

type Body = {
  title: string;
  date: Date;
  userId: User['id'];
};

type Response = Todo;

export class CreateTodoEndpoint implements Endpoint<{}, {}, {}, Body, Response> {
  method = EndpointMethod.POST;
  route = '/todos';
  schema: EndpointSchema = {
    tags: ['Todos'],
    summary: 'Creates a todo in the database',
    body: {
      type: 'object',
      required: ['title', 'date', 'userId'],
      additionalProperties: false,
      properties: {
        title: { type: 'string' },
        date: { type: 'string' },
        userId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private todoRepository: TodoRepository, private userRepository: UserRepository) {}

  async handler(request: EndpointHandlerInput<{}, {}, {}, Body>) {
    const todoData = request.body;
    await this.userRepository.read(todoData.userId);
    const newTodo: Todo = {
      id: v4(),
      createdAt: new Date(),
      updatedAt: new Date(),
      title: todoData.title,
      date: todoData.date,
      userId: todoData.userId,
    };
    const createdUser = await this.todoRepository.create(newTodo);
    return {
      status: 201,
      response: createdUser,
    };
  }
}
