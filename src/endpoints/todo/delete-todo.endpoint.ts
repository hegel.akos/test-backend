import { TodoRepository } from '../../domain/todo/todo.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';

type Params = {
  todoId: string;
};

export class DeleteTodoEndpoint implements Endpoint<{}, {}, Params, {}, {}> {
  method = EndpointMethod.DELETE;
  route = '/todos/:todoId';
  schema: EndpointSchema = {
    tags: ['Todos'],
    summary: 'Deletes todo from the database',
    params: {
      type: 'object',
      required: ['todoId'],
      additionalProperties: false,
      properties: {
        todoId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private todoRepository: TodoRepository) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, {}>) {
    const { todoId } = request.params;
    await this.todoRepository.delete(todoId);
    return {
      status: 204,
      response: {},
    };
  }
}
