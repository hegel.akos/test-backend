import { Todo } from '../../domain/todo/todo';
import { TodoRepository } from '../../domain/todo/todo.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';

type Params = { todoId: string };
type Response = Todo;

export class GetTodoEndpoint implements Endpoint<{}, {}, Params, {}, Response> {
  public method = EndpointMethod.GET;
  public route = '/todos/:todoId';
  public schema: EndpointSchema = {
    tags: ['Todos'],
    summary: 'Reads todo from the database based on ID',
    params: {
      type: 'object',
      required: ['todoId'],
      additionalProperties: false,
      properties: {
        todoId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private todoRepository: TodoRepository) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, {}>) {
    const { todoId } = request.params;
    const selectedTodo = await this.todoRepository.read(todoId);
    return {
      status: 200,
      response: selectedTodo,
    };
  }
}
