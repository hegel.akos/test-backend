import { Endpoint, EndpointMethod, EndpointSchema, EndpointHandlerInput } from '../../framework/endpoint/endpoint';
import { Todo } from '../../domain/todo/todo';
import { UserRepository } from '../../domain/user/user.repository';
import { TodoRepository } from '../../domain/todo/todo.repository';

type Params = { userId: string };
type Response = Todo[];

export class GetTodosByUserIdEndpoint implements Endpoint<{}, {}, Params, {}, Response> {
  method: EndpointMethod = EndpointMethod.GET;
  route = '/users/:userId/todos';
  schema: EndpointSchema = {
    summary: 'Returns all the todos connected to a user.',
    params: {
      type: 'object',
      additionalProperties: false,
      required: ['userId'],
      properties: {
        userId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private userRepository: UserRepository, private todoRepository: TodoRepository) {}

  public async handler(input: EndpointHandlerInput<{}, {}, Params, {}>) {
    const { userId } = input.params;
    await this.userRepository.read(userId);
    const todos = await this.todoRepository.readTodosByUserId(userId);
    return {
      status: 200,
      response: todos,
    };
  }
}
