import { Todo } from '../../domain/todo/todo';
import { TodoRepository } from '../../domain/todo/todo.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';

type Params = {
  todoId: string;
};

type Body = {
  title: string;
  date: Date;
};

type Response = Todo;

export class UpdateTodoEndpoint implements Endpoint<{}, {}, Params, Body, Response> {
  method = EndpointMethod.PATCH;
  route = '/todos/:todoId';
  schema: EndpointSchema = {
    tags: ['Todos'],
    params: {
      type: 'object',
      required: ['todoId'],
      additionalProperties: false,
      properties: {
        todoId: { type: 'string', format: 'uuid' },
      },
    },
    body: {
      type: 'object',
      additionalProperties: false,
      properties: {
        title: { type: 'string' },
        date: { type: 'string' },
      },
    },
  };

  constructor(private todoRepositort: TodoRepository) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, Body>) {
    const { todoId } = request.params;
    const todoUpdates = request.body;
    const updatedTodo = await this.todoRepositort.update(todoId, todoUpdates);
    return {
      status: 200,
      response: updatedTodo,
    };
  }
}
