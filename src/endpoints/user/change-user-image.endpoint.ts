import {
  Endpoint,
  EndpointMethod,
  EndpointSchema,
  EndpointHandlerInput,
  EndpointHandlerResponse,
} from '../../framework/endpoint/endpoint';
import { User } from '../../domain/user/user';
import { UserRepository } from '../../domain/user/user.repository';
import { ImageService } from '../../domain/image/image.service';

type Params = {
  userId: string;
};

type Body = {
  ref: string;
};

type Response = User;

export class ChangeUserImageEndpoint implements Endpoint<{}, {}, Params, Body, Response> {
  method: EndpointMethod = EndpointMethod.PATCH;
  route = '/users/:userId/images';
  schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Changes the image of the user.',
    params: {
      type: 'object',
      required: ['userId'],
      additionalProperties: false,
      properties: {
        userId: { type: 'string', format: 'uuid' },
      },
    },
    body: {
      type: 'object',
      required: ['ref'],
      additionalProperties: false,
      properties: {
        ref: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private userRepository: UserRepository, private imageService: ImageService) {}

  public async handler(
    request: EndpointHandlerInput<{}, {}, Params, Body>,
  ): Promise<EndpointHandlerResponse<Response>> {
    const { userId } = request.params;
    const { ref } = request.body;
    const selectedUser = await this.userRepository.read(userId);
    await this.imageService.check(ref);
    const updatedUser = await this.userRepository.update(selectedUser.id, { image: ref });
    if (selectedUser.image) {
      await this.imageService.delete(selectedUser.image);
    }
    return {
      status: 200,
      response: updatedUser,
    };
  }
}
