import { User } from '../../domain/user/user';
import { UserRepository } from '../../domain/user/user.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';
import { v4 } from 'uuid';
import { TransactionService } from '../../domain/transaction/transaction.service';

type Body = {
  email: string;
  name: string;
  phoneNumber: string;
};

type Response = User;

export class CreateUserEndpoint implements Endpoint<{}, {}, {}, Body, Response> {
  public method = EndpointMethod.POST;
  public route = '/users';
  public schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Creates a user in the database',
    body: {
      type: 'object',
      required: ['email', 'name', 'phoneNumber'],
      additionalProperties: false,
      properties: {
        email: { type: 'string', format: 'email' },
        name: { type: 'string' },
        phoneNumber: { type: 'string' },
      },
    },
  };

  constructor(private userRepository: UserRepository) {}

  public async handler(request: EndpointHandlerInput<{}, {}, {}, Body>) {
    const userData = request.body;
    const newUser: User = {
      id: v4(),
      createdAt: new Date(),
      updatedAt: new Date(),
      image: null,
      name: userData.name,
      email: userData.email,
      phoneNumber: userData.phoneNumber,
    };
    const createdUser = await this.userRepository.create(newUser);

    return {
      status: 201,
      response: createdUser,
    };
  }
}
