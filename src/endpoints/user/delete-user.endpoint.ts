import { UserRepository } from '../../domain/user/user.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';
import { TodoRepository } from '../../domain/todo/todo.repository';
import { ImageService } from '../../domain/image/image.service';

type Params = {
  userId: string;
};

export class DeleteUserEndpoint implements Endpoint<{}, {}, Params, {}, {}> {
  method = EndpointMethod.DELETE;
  route = '/users/:userId';
  schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Deletes user from the database',
    params: {
      type: 'object',
      required: ['userId'],
      additionalProperties: false,
      properties: {
        userId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(
    private userRepository: UserRepository,
    private todoRepository: TodoRepository,
    private imageService: ImageService,
  ) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, {}>) {
    const { userId } = request.params;
    const selectedUser = await this.userRepository.read(userId);
    if (selectedUser.image) {
      await this.imageService.delete(selectedUser.image);
    }
    await this.todoRepository.deleteTodosByUserId(userId);
    await this.userRepository.delete(userId);
    return {
      status: 204,
      response: {},
    };
  }
}
