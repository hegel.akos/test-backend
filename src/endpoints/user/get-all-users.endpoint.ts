import { User } from '../../domain/user/user';
import { UserRepository } from '../../domain/user/user.repository';
import { Endpoint, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';

type Response = User[];

export class GetAllUserEndpoint implements Endpoint<{}, {}, {}, {}, Response> {
  public method = EndpointMethod.GET;
  public route = '/users';
  public schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Reads all user from the database',
  };

  constructor(private userRepository: UserRepository, private domain: string) {}

  public async handler() {
    const selectedUsers = await this.userRepository.readAll();
    return {
      status: 200,
      response: selectedUsers,
    };
  }
}
