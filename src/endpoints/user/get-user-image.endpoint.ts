import {
  Endpoint,
  EndpointMethod,
  EndpointSchema,
  EndpointHandlerInput,
  EndpointHandlerResponse,
} from '../../framework/endpoint/endpoint';
import { ImageService } from '../../domain/image/image.service';

type Params = { userId: string; ref: string };
type Response = { url: string };

export class GetUserImageEndpoint implements Endpoint<{}, {}, {}, {}, Response> {
  method: EndpointMethod = EndpointMethod.GET;
  route = '/users/:userId/images/:ref';
  schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Return user image as url',
    params: {
      type: 'object',
      additionalProperties: false,
      required: ['userId', 'ref'],
      properties: {
        ref: {
          type: 'string',
          format: 'uuid',
        },
        userId: {
          type: 'string',
          format: 'uuid',
        },
      },
    },
  };

  constructor(private imageService: ImageService) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, {}>): Promise<EndpointHandlerResponse<Response>> {
    const { ref } = request.params;
    const image = await this.imageService.read(ref);
    return { status: 200, response: { url: image.signedUrl } };
  }
}
