import { User } from '../../domain/user/user';
import { UserRepository } from '../../domain/user/user.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';

type Params = { userId: string };
type Response = User;

export class GetUserEndpoint implements Endpoint<{}, {}, Params, {}, Response> {
  public method = EndpointMethod.GET;
  public route = '/users/:userId';
  public schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Reads user from the database based on ID',
    params: {
      type: 'object',
      required: ['userId'],
      additionalProperties: false,
      properties: {
        userId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private userRepository: UserRepository, private domain: string) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, {}>) {
    const { userId } = request.params;
    const selectedUser = await this.userRepository.read(userId);
    return {
      status: 200,
      response: selectedUser,
    };
  }
}
