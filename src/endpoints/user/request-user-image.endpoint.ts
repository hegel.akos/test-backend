import { v4 } from 'uuid';
import { ImageService } from '../../domain/image/image.service';
import { UserRepository } from '../../domain/user/user.repository';
import {
  Endpoint,
  EndpointHandlerInput,
  EndpointHandlerResponse,
  EndpointMethod,
  EndpointSchema,
} from '../../framework/endpoint/endpoint';

type Params = {
  userId: string;
};

type Response = { ref: string; signedUrl: string };

export class RequestUserImageEndpoint implements Endpoint<{}, {}, Params, Body, Response> {
  method: EndpointMethod = EndpointMethod.POST;
  route = '/users/:userId/images';
  schema: EndpointSchema = {
    tags: ['Users'],
    summary: 'Get signed url for user image upload',
    params: {
      type: 'object',
      required: ['userId'],
      additionalProperties: false,
      properties: {
        userId: { type: 'string', format: 'uuid' },
      },
    },
  };

  constructor(private userRepository: UserRepository, private imageService: ImageService) {}

  public async handler(
    request: EndpointHandlerInput<{}, {}, Params, Body>,
  ): Promise<EndpointHandlerResponse<Response>> {
    const { userId } = request.params;
    await this.userRepository.read(userId);
    const ref = v4();
    const { signedUrl } = await this.imageService.put(ref);
    return {
      status: 200,
      response: { signedUrl, ref },
    };
  }
}
