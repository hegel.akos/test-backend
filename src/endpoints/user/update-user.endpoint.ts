import { User } from '../../domain/user/user';
import { UserRepository } from '../../domain/user/user.repository';
import { Endpoint, EndpointHandlerInput, EndpointMethod, EndpointSchema } from '../../framework/endpoint/endpoint';

type Params = {
  userId: string;
};

type Body = {
  name: string;
  phoneNumber: string;
};

type Response = User;

export class UpdateUserEndpoint implements Endpoint<{}, {}, Params, Body, Response> {
  method = EndpointMethod.PATCH;
  route = '/users/:userId';
  schema: EndpointSchema = {
    tags: ['Users'],
    params: {
      type: 'object',
      required: ['userId'],
      additionalProperties: false,
      properties: {
        userId: { type: 'string', format: 'uuid' },
      },
    },
    body: {
      type: 'object',
      additionalProperties: false,
      properties: {
        name: { type: 'string' },
        phoneNumber: { type: 'string' },
      },
    },
  };

  constructor(private userRepository: UserRepository) {}

  public async handler(request: EndpointHandlerInput<{}, {}, Params, Body>) {
    const { userId } = request.params;
    const userUpdates = request.body;
    const updatedUser = await this.userRepository.update(userId, userUpdates);
    return {
      status: 200,
      response: updatedUser,
    };
  }
}
