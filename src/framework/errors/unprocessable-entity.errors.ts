import { APIError } from './errors';

export class UnprocessableEntity extends APIError {
  constructor(detail: string) {
    super(422, detail);
  }
}
