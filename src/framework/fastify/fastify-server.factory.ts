import fastify, { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from 'fastify';
import oas, { FastifyOASOptions } from 'fastify-oas';
import fastifyCors from 'fastify-cors';
import http from 'http';
import { Logger } from '../../logger';
import { Endpoint } from '../endpoint/endpoint';
import { APIError } from '../errors/errors';

export class FastifySever {
  private server: FastifyInstance;

  constructor(private endpoints: Endpoint[], private swaggerOptions: FastifyOASOptions, private logger: Logger) {
    this.server = fastify<http.Server>({
      logger: false,
    });
    this.setOAS();
    this.setCORS();
    this.buildServer();
  }

  private setOAS(): void {
    void this.server.register(oas, this.swaggerOptions);
  }

  private setCORS(): void {
    void this.server.register(fastifyCors, {});
  }

  private errorMapper(err: Error): { status: number; response: object } {
    if (err instanceof APIError) {
      return {
        status: err.status,
        response: {
          detail: err.detail,
        },
      };
    }
    this.logger.error(err);
    return {
      status: 500,
      response: { detail: err },
    };
  }

  private buildServer(): void {
    const wrappedFastifyHandler = (endpoint: Endpoint) => async (
      request: FastifyRequest,
      reply: FastifyReply<http.ServerResponse>,
    ) => {
      const start = Date.now();
      try {
        const endpointArgs = {
          headers: request.headers as {},
          query: request.query as {},
          params: request.params as {},
          body: request.body as {},
        };
        const endpointResult = await endpoint.handler(endpointArgs);
        this.logger.info({
          method: request.req.method,
          route: endpoint.route,
          duration: Date.now() - start,
          status: endpointResult.status,
        });
        const replyContentType = endpointResult.type || 'application/json';
        return reply.type(replyContentType).status(endpointResult.status).send(endpointResult.response);
      } catch (err) {
        const errorResponse = this.errorMapper(err);
        this.logger.error({
          method: request.req.method,
          route: endpoint.route,
          duration: Date.now() - start,
          status: errorResponse.status,
        });
        return reply.status(errorResponse.status).send(errorResponse.response);
      }
    };

    this.endpoints.forEach((endpoint) => {
      const fastifyRouteOptions: RouteOptions = {
        method: endpoint.method,
        url: endpoint.route,
        schema: endpoint.schema,
        handler: wrappedFastifyHandler(endpoint),
      };
      this.server.route(fastifyRouteOptions);
    });
  }

  public getServer(): FastifyInstance {
    return this.server;
  }
}
