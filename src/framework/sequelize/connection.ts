import { Sequelize } from 'sequelize';
import { createNamespace } from 'cls-hooked';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const sequilzerc = require('../../../.sequelizerc.js');

export const sequelizeConnection = (loggingEnabled: boolean): Sequelize => {
  const cls = createNamespace('sequelizeTransaction');
  Sequelize.useCLS(cls);
  const options = sequilzerc[process.env.NODE_ENV as string];
  return new Sequelize({
    ...options,
    logging: loggingEnabled ? console.log : false,
  });
};
