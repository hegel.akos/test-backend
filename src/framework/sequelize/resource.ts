import { Model, ModelAttributes, DataTypes, Sequelize, Transaction } from 'sequelize';

export type Resource = {
  id: string;
  createdAt: Date;
  updatedAt: Date;
};

export class ResourceModel extends Model {
  id!: string;
  createdAt!: Date;
  updatedAt: Date;
}

export const baseModelAttributes: ModelAttributes = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: Sequelize.literal('uuid_generate_v4()'),
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: new Date(),
  },
  updatedAt: {
    type: DataTypes.DATE,
    defaultValue: new Date(),
  },
};

export interface ResourceRepository<
  R extends Resource,
  Updates extends Omit<Resource, 'id' | 'createdAt' | 'updatedAt'>,
  Model extends ResourceModel = ResourceModel
> {
  transformer(model: Model): R;
  create(resource: Partial<Resource>): Promise<R>;
  read(id: R['id']): Promise<R>;
  readAll(): Promise<R[]>;
  update(id: R['id'], updates: Partial<Updates>): Promise<R>;
  delete(id: R['id']): Promise<void>;
}
