import { QueryInterface } from 'sequelize';
import { TODO_TABLE_NAME } from '../domain/todo/todo.model';
import { USERS_TABLE_NAME } from '../domain/user/user.model';
import { todoTableOptions } from './202007241200/todo.table';
import { userTableOptions } from './202007241200/user.table';

module.exports = {
  up: async (queryInterface: QueryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";', { transaction });
      await queryInterface.createTable(USERS_TABLE_NAME, userTableOptions, { transaction });
      await queryInterface.createTable(TODO_TABLE_NAME, todoTableOptions, { transaction });
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
  down: async (queryInterface: QueryInterface) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable(TODO_TABLE_NAME, { transaction });
      await queryInterface.dropTable(USERS_TABLE_NAME, { transaction });
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
