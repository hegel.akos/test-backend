import { DataTypes, ModelAttributes } from 'sequelize';
import { baseModelAttributes } from '../../framework/sequelize/resource';
import { USERS_TABLE_NAME } from '../../domain/user/user.model';
export const todoTableOptions: ModelAttributes = {
  ...baseModelAttributes,
  userId: {
    type: DataTypes.UUID,
    references: {
      model: USERS_TABLE_NAME,
      key: 'id',
    },
    allowNull: false,
  },
  title: {
    type: DataTypes.TEXT,
  },
  date: {
    type: DataTypes.DATE,
  },
};
