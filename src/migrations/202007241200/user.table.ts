import { DataTypes, ModelAttributes } from 'sequelize';
import { baseModelAttributes } from '../../framework/sequelize/resource';
export const userTableOptions: ModelAttributes = {
  ...baseModelAttributes,
  email: {
    type: DataTypes.TEXT,
    unique: true,
  },
  name: {
    type: DataTypes.TEXT,
  },
  phoneNumber: {
    type: DataTypes.TEXT,
  },
  image: {
    type: DataTypes.TEXT,
    allowNull: true,
    defaultValue: null,
  },
};
